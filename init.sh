#!/bin/sh
cd /var/www/html/
composer install
php artisan key:generate
php artisan migrate --seed
php artisan serve --port=80 --host=0.0.0.0
