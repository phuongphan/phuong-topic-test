<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class WagerTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('migrate');
    }

    /**
     * selling_price invalid
     *
     * @return void
     */
    public function testInvalidSellingPriceCreateWager()
    {
        $data = [
            'total_wager_value' => 500,
            'odds' => 33,
            'selling_percentage' => 50,
            'selling_price' => 300.011,
        ];

        $response = $this->json('POST', '/wagers', $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(['error']);
        $response->assertJsonFragment([
            "error" => "The selling price format is invalid.",
        ]);
    }

    /**
     * total_wager_value invalid
     *
     * @return void
     */
    public function testInvalidTotalWagerValueCreateWager()
    {
        $data = [
            'total_wager_value' => 0,
            'odds' => 33,
            'selling_percentage' => 50,
            'selling_price' => 300.00,
        ];

        $response = $this->json('POST', '/wagers', $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(['error']);
        $response->assertJsonFragment([
            "error" => "The total wager value must be at least 1.",
        ]);
    }

    /**
     * odds invalid
     *
     * @return void
     */
    public function testInvalidOddsCreateWager()
    {
        $data = [
            'total_wager_value' => 500,
            'odds' => 0,
            'selling_percentage' => 50,
            'selling_price' => 300.00,
        ];

        $response = $this->json('POST', '/wagers', $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(['error']);
        $response->assertJsonFragment([
            "error" => "The odds must be at least 1.",
        ]);
    }

    /**
     * odds selling_percentage
     *
     * @return void
     */
    public function testInvalidSellingPercentageCreateWager()
    {
        $data = [
            'total_wager_value' => 500,
            'odds' => 3,
            'selling_percentage' => 0,
            'selling_price' => 300.00,
        ];

        $response = $this->json('POST', '/wagers', $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(['error']);
        $response->assertJsonFragment([
            "error" => "The selling percentage must be at least 1.",
        ]);

        $data = [
            'total_wager_value' => 500,
            'odds' => 3,
            'selling_percentage' => 101,
            'selling_price' => 300.00,
        ];

        $response = $this->json('POST', '/wagers', $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(['error']);
        $response->assertJsonFragment([
            "error" => "The selling percentage must not be greater than 100.",
        ]);
    }

    /**
     * Unable to create a wager
     *
     * @return void
     */
    public function testUnableCreateWager()
    {
        $data = [
            'total_wager_value' => 500,
            'odds' => 33,
            'selling_percentage' => 50,
            'selling_price' => 249.00,
        ];

        $response = $this->json('POST', '/wagers', $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(['error']);
    }

    /**
     * Create a wager successful
     * 
     * @return void
     */
    public function testCreateWagerSuccessful()
    {
        $data = [
            'total_wager_value' => 500,
            'odds' => 33,
            'selling_percentage' => 50,
            'selling_price' => 260.00,
        ];

        $response = $this->json('POST', '/wagers', $data);
        $response->assertStatus(201);
        $response->assertJsonStructure([
            'id',
            'total_wager_value',
            'odds',
            'selling_percentage',
            'selling_price',
            'current_selling_price',
            'percentage_sold',
            'amount_sold',
            'placed_at',
        ]);
    }

    /**
     * Get wagers list
     * 
     * @return void
     */
    public function testGetWagers()
    {
        $response = $this->json('GET', '/wagers', [
            'paging' => 1,
            'limit' => 1,
        ]);
        $response->assertStatus(200);
        $response->assertJsonCount(0);

        // create new one
        $this->json('POST', '/wagers', [
            'total_wager_value' => 500,
            'odds' => 33,
            'selling_percentage' => 50,
            'selling_price' => 260.00,
        ]);
        
        // get listing again
        $response = $this->json('GET', '/wagers', [
            'paging' => 1,
            'limit' => 1,
        ]);
        $response->assertStatus(200);
        $response->assertJsonCount(1);
        $response->assertJsonStructure([
            '*' => [
                'id',
                'total_wager_value',
                'odds',
                'selling_percentage',
                'selling_price',
                'current_selling_price',
                'percentage_sold',
                'amount_sold',
                'placed_at',
            ]
        ]);
    }

    /**
     * Get wagers list - pagination
     * 
     * @return void
     */
    public function testGetWagersPagination()
    {
        // create new wagers
        $this->json('POST', '/wagers', [
            'total_wager_value' => 500,
            'odds' => 33,
            'selling_percentage' => 50,
            'selling_price' => 260.00,
        ]);
        $this->json('POST', '/wagers', [
            'total_wager_value' => 600,
            'odds' => 33,
            'selling_percentage' => 50,
            'selling_price' => 360.00,
        ]);
        $this->json('POST', '/wagers', [
            'total_wager_value' => 700,
            'odds' => 33,
            'selling_percentage' => 50,
            'selling_price' => 460.00,
        ]);
        $this->json('POST', '/wagers', [
            'total_wager_value' => 800,
            'odds' => 33,
            'selling_percentage' => 50,
            'selling_price' => 560.00,
        ]);
        $this->json('POST', '/wagers', [
            'total_wager_value' => 900,
            'odds' => 33,
            'selling_percentage' => 50,
            'selling_price' => 660.00,
        ]);
        
        // get listing again
        $response = $this->json('GET', '/wagers', [
            'page' => 2,
            'limit' => 2,
        ]);
        $response->assertStatus(200);
        $response->assertJsonCount(2);
        $response->assertJsonFragment([
            "id" => 3,
        ]);
        $response->assertJsonFragment([
            "id" => 2,
        ]);
    }

    /**
     * Buy wager faild
     * 
     * @return void
     */
    public function testBuyWagerFailed()
    {
        // create new one
        $wager = json_decode($this->json('POST', '/wagers', [
            'total_wager_value' => 500,
            'odds' => 33,
            'selling_percentage' => 50,
            'selling_price' => 260.00,
        ])->getContent());

        // buy wager
        $response = $this->json('POST', '/buy/' . $wager->id, [
            'buying_price' => 350,
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure(['error']);
    }

    /**
     * buying_wager invalid - Buy wager
     * 
     * @return void
     */
    public function testInvalidBuyingPriceBuyWager()
    {
        // create new one
        $wager = json_decode($this->json('POST', '/wagers', [
            'total_wager_value' => 500,
            'odds' => 33,
            'selling_percentage' => 50,
            'selling_price' => 260.00,
        ])->getContent());

        // buy wager
        $response = $this->json('POST', '/buy/' . $wager->id, [
            'buying_price' => 0,
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure(['error']);
    }

    /**
     * @return void
     */
    public function testInvalidBuyingPriceGreaterThanCurrentSellingPriceBuyWager()
    {
        // create new one
        $wager = json_decode($this->json('POST', '/wagers', [
            'total_wager_value' => 500,
            'odds' => 33,
            'selling_percentage' => 50,
            'selling_price' => 260.00,
        ])->getContent());

        // buy wager
        $response = $this->json('POST', '/buy/' . $wager->id, [
            'buying_price' => 260.01,
        ]);
        $response->assertStatus(200);
        $response->assertJsonStructure(['error']);
    }

    /**
     * Buy wager successful
     * 
     * @return void
     */
    public function testBuyWagerSuccessful()
    {
        // create new one
        $wager = json_decode($this->json('POST', '/wagers', [
            'total_wager_value' => 500,
            'odds' => 33,
            'selling_percentage' => 50,
            'selling_price' => 260.00,
        ])->getContent());

        // buy wager
        $response = $this->json('POST', '/buy/' . $wager->id, [
            'buying_price' => 130,
        ]);
        $response->assertStatus(201);
        $response->assertJsonStructure([
            'id',
            'wager_id',
            'buying_price',
            'bought_at',
        ]);
    }

    /**
     * Buy wager values successful
     * 
     * @return void
     */
    public function testBuyWagerValuesSuccessful()
    {
        // create new one
        $wager = json_decode($this->json('POST', '/wagers', [
            'total_wager_value' => 500,
            'odds' => 33,
            'selling_percentage' => 50,
            'selling_price' => 260.00,
        ])->getContent());

        // buy wager
        $response = $this->json('POST', '/buy/' . $wager->id, [
            'buying_price' => 130,
        ]);
        $response->assertStatus(201);
        $response->assertJsonStructure([
            'id',
            'wager_id',
            'buying_price',
            'bought_at',
        ]);
        
        // get listing
        $response = $this->json('GET', '/wagers', [
            'paging' => 1,
            'limit' => 1,
        ]);
        $response->assertStatus(200);
        $response->assertJsonCount(1);
        $response->assertJsonStructure([
            [
                'id',
                'total_wager_value',
                'odds',
                'selling_percentage',
                'selling_price',
                'current_selling_price',
                'percentage_sold',
                'amount_sold',
                'placed_at',
            ]
        ]);

        // check values
        $response->assertJsonFragment([
            "id" => 1,
            "total_wager_value" => 500,
            "odds" => 33,
            "selling_percentage" => 50,
            "selling_price" => 260,
            "current_selling_price" => 130,
            "percentage_sold" => 50,
            "amount_sold" => 130,
        ]);
    }
}
