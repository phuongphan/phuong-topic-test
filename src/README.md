## How To Work

- Build and start docker: `start.sh launch`
- Down docker: `start.sh down`

## Unit Test
- Going to container php `docker exec -it php bash`
- Run: `php artisan test`
