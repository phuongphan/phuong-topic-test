<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wagers', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->double('total_wager_value', 8, 2);
            $table->float('odds', 8, 2);
            $table->integer('selling_percentage');
            $table->double('selling_price', 8, 2);
            $table->double('current_selling_price', 8, 2);
            $table->integer('percentage_sold')->nullable();
            $table->integer('amount_sold')->nullable();
            $table->timestamp('placed_at')->useCurrent();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wagers');
    }
};
