<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_wagers', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->foreignId('wager_id')
                ->constrained()
                ->onDelete('cascade');
            $table->double('buying_price', 8, 2);
            $table->timestamp('bought_at')->useCurrent();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buy_wagers');
    }
};
