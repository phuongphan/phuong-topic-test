<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\Wager;
use App\Http\Resources\Wager as WagerResource;

class WagerController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $wagers = Wager::orderBy('id', 'desc')->paginate($request->limit ?? 10);
        return response()->json(
            WagerResource::collection($wagers),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'total_wager_value' => ['required', 'numeric', 'min:1'],
            'odds' => ['required', 'numeric', 'min:1'],
            'selling_percentage' => ['required', 'numeric', 'min:1', 'max:100'],
            'selling_price' => [
                'required',
                'regex:/^\d{1,13}(\.\d{1,2})?$/',
                function ($attribute, $value, $fail) use ($input) {
                    if (is_numeric($input['selling_percentage'])) {
                        $selling_price = $input['total_wager_value'] * ($input['selling_percentage'] / 100);
                        $selling_price_compare = (int) ($selling_price * 10000);
                        $buying_price = (int) ($value * 10000);
                        if ($buying_price <= $selling_price_compare) {
                            $fail('The '.$attribute.' must be greater than ' . $selling_price . '.');
                        }
                    }
                },
            ],
        ]);
        if ($validator->fails()){
            return response()->json([
                    'error' => $validator->errors()->first(),
                ],
                200
            );
        }
        $wager = Wager::create($input);
        return response()->json(
            new WagerResource($wager),
            201
        );
    }
}
