<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\Wager;
use App\Models\BuyWager;
use App\Http\Resources\BuyWager as BuyWagerResource;

class BuyWagerController extends Controller
{
    /**
     * Buy wager
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, int $id)
    {
        $request->merge(['wager_id' => $id]);
        $input = $request->all();
        $validator = Validator::make($input, [
            'wager_id' => ['required', 'numeric', 'exists:wagers,id'],
            'buying_price' => [
                'required',
                'regex:/^\d{1,13}(\.\d{1,2})?$/',
                function ($attribute, $value, $fail) use ($input, $id) {
                    if ($value < 1) {
                        $fail('The '.$attribute.' must be at least 1.');
                    } else {
                        $wager = Wager::find($id);
                        $current_selling_price = (int) ($wager->current_selling_price * 10000);
                        $buying_price = (int) ($value * 10000);
                        if ($wager && $current_selling_price < $buying_price) {
                            $fail('The '.$attribute.'  lesser or equal to ' . $wager->current_selling_price . '.');
                        }
                    }
                },
            ],
        ]);
        if ($validator->fails()){
            return response()->json([
                    'error' => $validator->errors()->first(),
                ],
                200
            );
        }
        $wager = BuyWager::create($input);
        return response()->json(
            new BuyWagerResource($wager),
            201
        );
    }
}
