<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wager extends Model
{
    use HasFactory;
    protected $fillable = ['total_wager_value', 'odds', 'selling_percentage', 'selling_price', 'current_selling_price', 'percentage_sold', 'amount_sold', 'placed_at'];

    public static function boot() {
        parent::boot();
    
        self::creating(function ($model) {
            $model->current_selling_price = $model->selling_price;
            $model->placed_at = date('Y-m-d H:i:s');
        });
    }

    public function buy_wagers()
    {
        return $this->hasMany(BuyWager::class);
    }

    public function calcSellingPrice($buying_price)
    {
        $this->current_selling_price -= $buying_price;
        $this->amount_sold += $buying_price;
        $this->percentage_sold = $this->amount_sold / $this->selling_price * 100;
        return $this->save();
    }
}
