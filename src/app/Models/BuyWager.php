<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BuyWager extends Model
{
    use HasFactory;
    protected $fillable = ['wager_id', 'buying_price', 'bought_at'];

    public static function boot() {
        parent::boot();
    
        self::creating(function ($model) {
            $model->bought_at = date('Y-m-d H:i:s');
        });
    
        self::created(function ($model) {
            $model->wager->calcSellingPrice(
                $model->buying_price
            );
        });
    }
    
    public function wager()
    {
        return $this->belongsTo(Wager::class, 'wager_id');
    }
}
