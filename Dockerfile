FROM php

RUN apt-get clean && apt-get update \
    &&  apt-get install -y --no-install-recommends \
        locales \
        apt-utils \
        git \
        libicu-dev \
        g++ \
        libpng-dev \
        libxml2-dev \
        libzip-dev \
        libonig-dev \
        libxslt-dev \
        unzip \
        libpq-dev \
        nodejs \
        npm \
        wget \
        apt-transport-https \
        lsb-release \
        ca-certificates

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen  \
    && locale-gen

RUN curl -sS https://getcomposer.org/installer | php -- \
    &&  mv composer.phar /usr/local/bin/composer

RUN docker-php-ext-configure intl && docker-php-ext-install \
    pdo pdo_mysql pdo_pgsql opcache intl zip calendar dom mbstring gd xsl

RUN pecl install apcu && docker-php-ext-enable apcu

RUN npm install --global yarn

WORKDIR /var/www/html/

# Copy the source code and prepare necessary data for laravel
COPY ./src ./
COPY ./src/.env.example ./.env
COPY init.sh ./init.sh
RUN touch ./database/db-test.sqlite
RUN chmod +x ./init.sh
CMD ["./init.sh"]

# RUN composer install
# RUN php artisan key:generate
# RUN php artisan migrate --seed
# RUN php artisan serve --port=80 --host=0.0.0.0
