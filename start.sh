#!/bin/sh

IMAGES="mariadb:10.6.4-focal prophet-challenge_php"
HOST=0.0.0.0
PORT=80

err() {
    echo $* >&2
}

usage() {
    err "$(basename $0): [up|down|clean]"
}

clean() {
    for IMAGE in $IMAGES
    do
        docker rm $IMAGE
    done
}

down() {
    docker-compose down
}

up() {
    docker-compose up -d
}

launch() {
    docker-compose build
    docker-compose up -d
}

execute() {
    local task=${1}
    case ${task} in
        clean)
            clean
            ;;
        down)
            down
            ;;
        up)
            up
            ;;
        launch)
            launch
            ;;
        *)
            err "invalid task: ${task}"
            usage
            exit 1
            ;;
    esac
}

main() {
    [ $# -ne 1 ] && { usage; exit 1; }
    local task=${1}
    execute ${task}
}

main $@
